(uiop:define-package :nbd/lib/server
    (:use
     :cl
     :nbd/lib/shared
     :nbd/lib/handshake
     :nbd/lib/transmission
     :nbd/lib/transmission-flags)
  (:import-from :bordeaux-threads #:make-thread #:join-thread)
  (:import-from :sb-bsd-sockets
                #:local-socket
                #:socket-accept
                #:socket-bind
                #:socket-close
                #:socket-listen
                #:socket-make-stream
                #:sockopt-tcp-nodelay)
  (:export #:start
           #:wait
           #:*debug-mode*))

(in-package :nbd/lib/server)

(defvar *debug-mode* nil)

(defvar *unsupported-flags*
  (list *send-write-zeroes*
        *send-df*
        *can-multi-conn*
        *send-resize*
        *send-cache*
        *send-fast-zero*))

(defun start (on-request path export-name export-size supported-flags
              &optional export-description (backlog 100) (min-block-size 1)
                (preferred-block-size 4096) (max-block-size nil))
  (dolist (flag supported-flags)
    (dolist (unsupported-flag *unsupported-flags*)
      (when (logtest unsupported-flag flag)
        (error (format nil "~a is currently not a supported flag." flag)))))
  (let* ((socket-type 'local-socket)
         (socket (make-instance socket-type
                                :type :stream))
         (server (make-instance 'server
                                :on-request on-request
                                :socket socket
                                :stream (socket-make-stream socket
                                                            :element-type '(unsigned-byte 8)
                                                            :output t
                                                            :input t)
                                :export-name export-name
                                :export-description (or export-description export-name)
                                :export-size export-size
                                :min-block-size min-block-size
                                :preferred-block-size preferred-block-size
                                :max-block-size (or max-block-size export-size)
                                :supported-flags (apply #'logior 0 supported-flags))))
    (unless (eql socket-type 'local-socket)
      ;; recommended by the spec, unsupported for now.
      (setf (sockopt-tcp-nodelay socket) t))

    (socket-bind socket path)
    (socket-listen socket backlog)

    (setf (server-accept-thread server)
          (make-thread (lambda () (accept-loop server))
                       :name "NBD Accept Loop"
                       :initial-bindings `((server . ,server))))

    server))

(defun wait (server)
  (join-thread (server-accept-thread server)))

(defmethod accept-loop ((server server))
  (let ((server-socket (server-socket server)))
    (loop
      (let ((client-socket (socket-accept server-socket)))
        (make-thread (lambda ()
                       (handle-client-socket
                        server
                        (make-instance 'client
                                       :socket client-socket
                                       :stream (socket-make-stream
                                                client-socket
                                                :element-type '(unsigned-byte 8)
                                                :output t
                                                :input t))))
                     :initial-bindings `((server . ,server)
                                         (client-socket . ,client-socket))
                     :name (format nil "Handle client socket ~a" client-socket))))))

(defmethod handle-client-socket ((server server) client)
  (flet ((serve-client ()
           (handshake server client)
           (transmission server client)))
    (unwind-protect
         (if *debug-mode*
             (serve-client)
             (handler-case
                 (serve-client)
               (error (e)
                 (format t "Closing client socket: ~a~%" e))))
      (socket-close (client-socket client)))))
