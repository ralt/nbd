(uiop:define-package :nbd/lib/transmission-flags
    (:use :cl)
  (:export #:*has-flags*
           #:*read-only*
           #:*send-flush*
           #:*send-fua*
           #:*rotational*
           #:*send-trim*
           #:*send-write-zeroes*
           #:*send-df*
           #:*can-multi-conn*
           #:*send-resize*
           #:*send-cache*
           #:*send-fast-zero*))

(in-package :nbd/lib/transmission-flags)

(defvar *has-flags* #b1
  "Is always be set by the server.")
(defvar *read-only* #b10
  "If the export is read-only. The server will always return EPERM on writes if set.")
(defvar *send-flush* #b100
  "Supports CMD_FLUSH. The server must flush all data to persistent storage before replying.")
(defvar *send-fua* #b1000
  "Supports CMD_FLAG_FUA. The server must flush all data to persistent storage before handling the request.")
(defvar *rotational* #b10000
  "Has rotational disk characteristics, IOW doesn't do random access.")
(defvar *send-trim* #b100000
  "Supports CMD_TRIM. This is a hint that the server can discard the data.")
(defvar *send-write-zeroes* #b1000000
  "Currently unsupported. Supports CMD_WRITE_ZEROES and CMD_FLAG_NO_HOLE.")
(defvar *send-df* #b10000000
  "Currently unsupported. Supports CMD_FLAG_DF.")
(defvar *can-multi-conn* #b100000000
  "Currently unsupported. Resources are shared across clients. Influences the behavior of CMD_FLUSH and CMD_FLAG_FUA on the server side.")
(defvar *send-resize* #b1000000000
  "Currently unsupported. Supports resizing.")
(defvar *send-cache* #b10000000000
  "Currently unsupported. Supports CMD_CACHE, aka posix_fadvise().")
(defvar *send-fast-zero* #b100000000000
  "Currently unsupported. Supports CMD_FLAG_FAST_ZERO.")
