(uiop:define-package :nbd/simple-in-memory/main
    (:use :cl)
  (:import-from :bordeaux-threads #:make-lock #:with-lock-held)
  (:import-from :nbd/api))

(in-package :nbd/simple-in-memory/main)

(defvar *export-name* "test")
(defvar *export-size* (* 100 1024 1024))
(defvar *data* (make-array *export-size* :element-type '(unsigned-byte 8)))
(defvar *lock* (make-lock))

(defun main (path)
  (nbd/api:wait (nbd/api:start #'on-request path *export-name* *export-size*
                               (list nbd/api:*send-flush*
                                     nbd/api:*send-trim*))))

(defun on-request (type flags handle offset length stream start-reply)
  (declare (ignore flags handle))
  ;; Each client is single-threaded but the server can have more than
  ;; one client.
  (with-lock-held (*lock*)
    (case type
      (:read
       (funcall start-reply)
       (write-sequence *data* stream :start offset :end (+ offset length)))
      (:write
       (read-sequence *data* stream :start offset :end (+ offset length))
       (funcall start-reply))
      (:flush
       ;; everything is in-memory, nothing to flush.
       (funcall start-reply))
      (:trim
       ;; this is a hint, IOW we can ignore it.
       (funcall start-reply)))
    (finish-output stream)))
